/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ventascooter;

/**
 *
 * @author
 */
public class VentaScooter {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Puesto pueston = new Puesto(1, "gerente");
        Puesto puestito = new Puesto(2, "ejecutivo");

        Empleado empleado1 = new Empleado("1-9", "john", 'M', 10, 40, pueston);
        Empleado empleado2 = new Empleado("2-7", "juanita", 'F', 2, 25, puestito);

        Empresa empresa = new Empresa(); // crea la colección en el constructor

        //agregar empleado1
        if (empresa.buscarEmpleado("1-9") == false) {
            empresa.agregar(empleado1);
            System.out.println("Se agregó empleado " + empleado1.getNombreEmpleado());
        } else {
            System.out.println("Empleado exite");
        }

        //agregar empleado2
        if (empresa.buscarEmpleado("2-7") == false) {
            empresa.agregar(empleado2);
            System.out.println("Se agregó empleado " + empleado2.getNombreEmpleado());
        } else {
            System.out.println("Empleado exite");
        }

        //listar empleados
        empresa.listarEmpleados();

        //eliminar 
        if (empresa.eliminarEmpleado("2-7")) {
            System.out.println("Se eliminó empleado " + empleado1.getNombreEmpleado());
        } else {
            System.out.println("No se eliminó empleado " + empleado1.getNombreEmpleado());
        }
        
         empresa.listarEmpleados();
         

//         empresa.modificarEdadEmpleado(41, "1-9");
//         System.out.println(empleado1.getEdad());

    }

}
